### Quick workaround for blocked proxy settings caused by a GPO.

![Screenshot](https://gitlab.com/cyberdos/proxy-settings-unblocker/-/raw/master/docs/Screenshot1.PNG)

![Screenshot](https://gitlab.com/cyberdos/proxy-settings-unblocker/-/raw/master/docs/Screenshot2.PNG)