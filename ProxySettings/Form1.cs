﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProxySettings
{
    public partial class Form1 : Form
    {
        private RegistryKey key;

        public Form1()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Internet Settings", true);
            bool enabled = ((int)key.GetValue("ProxyEnable")) == 1;
            string[] server = ((string)key.GetValue("ProxyServer")).Split(':');
            string address = server[0];
            string port = server[1];
            
            this.checkBox_enabled.Checked = enabled;
            updateCheckbox();
            this.textBox_address.Text = address;
            this.textBox_port.Text = port;
        }

        private void updateCheckbox()
        {
            this.label_address.Enabled = this.checkBox_enabled.Checked;
            this.label_port.Enabled = this.checkBox_enabled.Checked;
            this.textBox_address.Enabled = this.checkBox_enabled.Checked;
            this.textBox_port.Enabled = this.checkBox_enabled.Checked;
        }

        private void checkBox_enabled_CheckedChanged(object sender, EventArgs e)
        {
            updateCheckbox();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            key.SetValue("ProxyEnable", this.checkBox_enabled.Checked ? 1 : 0);
            key.SetValue("ProxyServer", this.textBox_address.Text + ":" + this.textBox_port.Text);
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.checkBox_enabled.Checked = !this.checkBox_enabled.Checked;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.checkBox_enabled.Checked = !this.checkBox_enabled.Checked;
        }
    }
}
